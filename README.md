## Get links from webpage

Gets all links from a webpage, keep forgetting this.

    links = getLinks("yoursite.com")

This includes all links, including mailto and without root.
They are scraped as written in the href tag.

## Development

Developed in Python,

* https://pythonbasics.org/
* https://pythonprogramminglanguage.com/

